function exercise2(){
    var celsius = document.getElementById("celsius")
    var ferenheit = document.getElementById("ferenheit")

    celsius.addEventListener("change", function(){
        celsius=celsius.value
        ferenheit.value=celsius*9/5+32
        celsius = document.getElementById("celsius")
    })
    ferenheit.addEventListener("change", function(){
        ferenheit=ferenheit.value
        celsius.value = (ferenheit-32)*5/9
        ferenheit = document.getElementById("ferenheit")
    })
}
exercise2()