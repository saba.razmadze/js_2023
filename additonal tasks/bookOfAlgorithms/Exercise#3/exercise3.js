function exercise3(){
    var number = document.getElementById("number")
    var result = document.getElementById("result")

    number.addEventListener("change", function(){
        number=number.value
        if (number&2==2){
            result.innerHTML=number+" is odd"
        }else{
            result.innerHTML=number+" is even"
        }
        number = document.getElementById("number")
    })
}
exercise3()