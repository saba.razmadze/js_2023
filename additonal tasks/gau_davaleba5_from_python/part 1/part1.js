function task1_volume_of_cilinder(r,h){
    v = (Math.PI.toFixed(2))*r*h
    document.write("task 1 <br> ცილინდრის სიმაღეა h = "+h+"<br> რადიუსი r = " +r+"<br> ხოლო მოცულობა ამ ფორმულით V = πr<sup>2</sup>h  =  "+v)
}
task1_volume_of_cilinder(2,3)

function task2_area_of_triangular(a,h){
    s = 1/2*a*h
    document.write("<br><br><br> task 2 <br> სამკუთხედის გვერდი a = "+a+"<br>  სამკუთხედის სიმაღლე h = " +h+"<br> ხოლო ფართობი ამ ფორმულით S = ½ * a * h =  "+s)
}
task2_area_of_triangular(4,3)

function task3_area_of_triangular(a,b,c){
    p = (a + b + c)/2
    s = Math.sqrt(p*(p-a)*(p-b)*(p-c))
    document.write("<br><br><br> task 3 <br> სამკუთხედის გვერდი a = "+a+" <br>სამკუთხედის გვერდი b = "+b+"<br>სამკუთხედის გვერდი c = "+c+"<br> ხოლო ფართობი ჰერონის ფორმულით S ="+s.toFixed(2))
}
task3_area_of_triangular(4,4,4)

function task4_area_of_triangular(a,b,c,R){
    s = a*b*c/(4*R)
    document.write("<br><br><br> task 4 <br> სამკუთხედის გვერდი a = "+a+" <br>სამკუთხედის გვერდი b = "+b+"<br>სამკუთხედის გვერდი c = "+c+"<br>სამკუთხედიზე შემოხაზული წრეწირის რადიუსი R = "+R+"<br> ხოლო ფართობი S ="+s.toFixed(2))
}
task4_area_of_triangular(4,4,4,4)

function task5_area_of_triangular(a,b,c,r){
    p = (a + b + c)/2
    s = p*r
    document.write("<br><br><br> task 5 <br> სამკუთხედის გვერდი a = "+a+" <br>სამკუთხედის გვერდი b = "+b+"<br>სამკუთხედის გვერდი c = "+c+"<br>სამკუთხედიში ჩახაზული წრეწირის რადიუსი r = "+r+"<br> ხოლო ფართობი S ="+s.toFixed(2))
}
task5_area_of_triangular(4,4,4,2)

function task6_area_of_square(a){
    s = a*a
    document.write("<br><br><br> task 6 <br> კვადრატის გვერდი a = "+a+"<br> ხოლო ფართობი S ="+s)
}
task6_area_of_square(4)

function task7_area_of_square(d){
    s = d*d/2
    document.write("<br><br><br> task 7 <br> კვადრატის დიაგონალი d = "+d+"<br> ხოლო ფართობი S ="+s)
}
task7_area_of_square(4)

function task8_area_of_rectangular(a,b){
    s = a*b
    document.write("<br><br><br> task 8 <br> მართკუთხედის გვერდი a = "+a+"<br> მართკუთხედის გვერდი b = "+b+"<br> ხოლო ფართობი S ="+s)
}
task8_area_of_rectangular(4,7)

function task9_area_of_paralelogram(a,h){
    s = a*h
    document.write("<br><br><br> task 9 <br> პარალელოგრამის გვერდი a = "+a+"<br> პარალელოგრამის სიმაღლე h = "+h+"<br> ხოლო ფართობი S ="+s)
}
task9_area_of_paralelogram(7, 4)


function task10_area_of_paralelogram(a,b,alfa){
    alfa2 = alfa*(Math.PI/180)
    s = a*b*Math.sin(alfa2)
    document.write("<br><br><br> task 10 <br> პარალელოგრამის გვერდი a = "+a+"<br>პარალელოგრამის გვერდი b = "+b+"<br>პარალელოგრამის გვერდებს შორის კუთხე არის "+alfa+" გრასუსი <br> ხოლო ფართობი S ="+s.toFixed(3))
}
task10_area_of_paralelogram(7, 4.5, 30)


function task11_area_of_romb_a(a,h){
    s = a*h
    document.write("<br><br><br> task 11_a <br> რომბის გვერდი a = "+a+"<br> რომბის სიმაღლე h = "+h+"<br> ხოლო ფართობი S ="+s)
}
task11_area_of_romb_a(2,3)

function task11_area_of_romb_b(a,alfa){
    alfa2 = alfa*(Math.PI/180)
    s = a*a*Math.sin(alfa2)
    document.write("<br><br><br> task 11_b <br> რომბის გვერდი a = "+a+"<br> რომბის გვერდებს შორის კუთხე არის "+alfa+" გრასუსი <br>ხოლო ფართობი S ="+s.toFixed(2))
}
task11_area_of_romb_b(2,30)

function task11_area_of_romb_c(d_1,d_2){
    s=d_1*d_2/2
    document.write("<br><br><br> task 11_c <br> რომბის დიაგონალი d1 = "+d_1+"<br> რომბის გდიაგონალი d2 = "+d_2+"<br>ხოლო ფართობი S ="+s.toFixed(2))
}
task11_area_of_romb_c(2,30)

function task12(a,b,c,d){
    p = (a + b + c)/2
    s=((a+b)*Math.sqrt(p*(p-a)*(p-b)*(p-a-c)*(p-a-d))/(4*Math.abs(a-b)))
    document.write("<br><br><br> task 12 <br> ტრაპეციის გვერდი a = "+a+"<br>ტრაპეციის გვერდი b = "+b+"<br>ტრაპეციის გვერდი c = "+c+"<br>ტრაპეციის გვერდი d = "+d+"<br>ხოლო ფართობი S ="+s.toFixed(2))
}
task12(1,2,5,8)

function task13_a(r){
    s=Math.PI*r*r
    document.write("<br><br><br> task 13_a <br> წრის რადიუსია r ="+r+"<br>ხოლო ფართობი S ="+s.toFixed(2))
}
task13_a(5)

function task13_b(d){
    s=Math.PI*d*d/4
    document.write("<br><br><br> task 13_b <br> წრის დიაგონალი d ="+d+"<br>ხოლო ფართობი S ="+s.toFixed(2))
}
task13_b(10)

function task14(a,b){
    s=Math.PI*a*b
    document.write("<br><br><br> task 14 <br> ელიფსის სიგრძე? a ="+a+"<br> ელიფსის სიგანე? b ="+b+"<br>ხოლო ფართობი S ="+s.toFixed(2))
}
task14(10,8)