function task1(x){
    result = 2*x**3+3*x**2-5*x-10
    document.write("task1 <br> x = "+x+"<br> 2x<sup>3</sup>+3x<sup>2</sup>-5x-10 = " + result)
}

task1(4)

function task2(x){
    result1 = x**3-4*x**2+5*x-7
    result2 = 0.5*x**2+2.5*x+12
    document.write("<br><br><br>task2-A <br> x = "+x+"<br> x<sup>3</sup>-4x<sup>2</sup>+5x-7 = " + result1 +"<br><br>task2-B <br> x = "+x+"<br> 0.5x<sup>2</sup>+2.5x+12 = " + result2)
}
task2(10)

//---- task 3 goes here -----

function task4(n,k,m,){
    document.write("<br><br><br> task 4<br> n = "+n+"<br> k = "+k+"<br> m = "+m+"<br><br>")

    if (Number.isInteger(n)){
        if(n>0){
            document.write("a)  n დადებითი მთელი რიცხვია<br>")
        }else{
            document.write("a) n უარყოფითი მთელი რიცხვია<br>")
        }
    } else{
        document.write("a) n მთელი რიცხვი არაა<br>")
    }

    if (Number.isInteger(k)){
        if (k%5==0 || k%7==0){
            document.write("b) k 5 ის და 7 ის ჯერადია ")
        } else{
            document.write("b) k 5 ის და 7 ის ჯერადი არ არის")
        }
    } else{
        document.write("b) k მთელი რიცხვი არაა")
    }

    if (Number(m) === m && m % 1 !== 0){
        if (m<10){
            document.write("<br> c) m ნამდვილია და ნაკლებია ათზე")
        } else{
            document.write("<br> c) m ნამდვილია და მეტია 10 ზე")
        }
    } else{
        document.write("<br> c) m ნამდვილი რიცხვი არაა")
    }
    //---- აქ გავჩერდი
}
task4(3, 7, 45.3)

