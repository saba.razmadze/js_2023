function task1(text){
    document.write("<i><u><b>"+text)
}

task1("rame")

function task2(number1, number2){
    document.write("<br>"+number1+"+"+number2+"="+(number1+number2))
}

task2(5,5)

function task3(text, fontSize){
    document.write('<p style=" font-size: '+fontSize+'px;">'+text+'</p>')
   }
   
task3("text font size 70",70)  

function task4(width){
    document.write('<table  border="1" style="width:'+width+'px; border-collapse: collapse;"><tr><td>1</td><td>2</td></tr><tr><td>3</td><td>4</td></tr></table>')
   }
   
task4(500)  

function task5(height){
    document.write('<br><table  border="1" style="height:'+height+'px; border-collapse: collapse;"><tr><td>1</td><td>2</td></tr><tr><td>3</td><td>4</td></tr></table>')
   }
   
task5(100)  

function task6(width, height){
    document.write('<br><table  border="1" style="height:'+height+'px; width:'+width+'px; border-collapse: collapse;"><tr><td>1</td><td>2</td></tr><tr><td>3</td><td>4</td></tr></table>')
   }
   
task6(400,100) 

function task7(color){
    document.write('<br><table  border="1" style="height: 200px; width:200px; border-collapse: collapse; background-color: '+color+';"><tr><td>1</td><td>2</td></tr><tr><td>3</td><td>4</td></tr></table>')
   }
   
task7("green") 


function task8(borderSize){
    document.write('<br><table  border="1" style="height: 200px; width:200px;border-collapse: collapse; border-width: '+borderSize+'px;"><tr><td>1</td><td>2</td></tr><tr><td>3</td><td>4</td></tr></table>')
   }
   
task8(20) 

function task9(borderSize,color,width, height){
    document.write('<br><table  border="1" style="height: 200px; width:200px;border-collapse: collapse;height:'+height+'px; width:'+width+'px;collapse; background-color: '+color+'; border-width: '+borderSize+'px;"><tr><td>1</td><td>2</td></tr><tr><td>3</td><td>4</td></tr></table>')
   }
   
task9(20,"green", 400, 100) 

function task10(text){
    document.write('<br><table  border="1" style="height: 200px; width:200px;border-collapse: collapse;"><tr><td>'+text+'</td><td>'+text+'</td></tr><tr><td>'+text+'</td><td>'+text+'</td></tr></table>')
   }
   
task10("some random text") 

function task11(){
    document.write('<br> natural numbers 1-10 are - ')
    for ( i=1; i<11; i++){
        document.write(i)
    }
}

task11()

function task12(n){
    document.write('<br> natural numbers form 1-'+n+' are - ')
    for ( i=1; i<n+1; i++){
        document.write('<br>'+i)
    }
}

task12(5)

function task13(m,n){
    document.write('<br> natural numbers form '+m+'-'+n+' are - ')
    for ( i=m; i<n+1; i++){
        document.write('<br>'+i)
    }
}

task13(5,8)

function task14(m,n){
    if (m<n){
        document.write('<br> natural numbers form '+m+'-'+n+' are - ')
        for ( i=m; i<n+1; i++){
            document.write('<br>'+i)
        }
    } else{
        document.write('<br> natural numbers form '+n+'-'+m+' are - ')
        for ( i=n; i<m+1; i++){
            document.write('<br>'+i)
        }
    }
}

task14(8,6)
