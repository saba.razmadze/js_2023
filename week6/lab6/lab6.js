function task1(){
    alert("გამარჯობა მე ვარ დიალოგური ფანჯარა")
    document.getElementById("span1").innerHTML="კეთილი იყოს თქვენი მობრძანება საიტზე"
}
// task1()

function task2() {
    if (confirm("გამარჯობა მე ვარ დიალოგური ფანჯარა") == true) {
        document.getElementById("span1").innerHTML="გილოცავთ სწორი არჩევანი გააკეთეთ"
    } else {
        document.getElementById("span1").innerHTML="დაინტერესდით ვებ გევერდის აწყობით"
    }
}
// task2()

function task3() {
    let days=["ორშაბათი","სამშაბათი","ოთხშაბათი","ხუთშაბათი","პარასკევი","შაბათი","კვირა"]
    let a=prompt("შემოიტანე კვირის დღე [1 2 3 4 5 6 7]")
    if (a>=1&a<=7){
        a--
        document.getElementById("span1").innerHTML="დღეს არის "+days[a]
    }else if (a==null){
        document.getElementById("span1").innerHTML="თქვენ დააჭირეთ cancel - ზე"
    }else{
        document.getElementById("span1").innerHTML="ესეთი დღე არ არის"
    }

}
// task3()

function task4(){
    let t=new Date()
    let textplace= document.getElementById("span1")
    let year= t.getFullYear()
    let weekday=t.getDay()
    let day=t.getDate()
    let month=t.getMonth()
    let hoour=t.getHours()
    let minutes=t.getMinutes()
    let second=t.getSeconds()
    let milisecond= t.getMilliseconds()

    let days=["კვირა","ორშაბათი","სამშაბათი","ოთხშაბათი","ხუთშაბათი","პარასკევი","შაბათი"]
    let months=["იანვარი","თებერვალი","მარტი","აპრილი","მაისი","ივნისის","ივლისი","აგვისტო","სექტემბერი","ოქტომბერი","ნოემბრი","დეკემბერი"];

    finaltext="დღეს არის "+year+" წლის "+months[month]+"ის "+day+" "+days[weekday]+" "+hoour+" საათი, "+minutes+" წთ, "+second+" წამი " + milisecond+" მილიწამი"

    textplace.innerHTML=finaltext
}
// task4()

function task5(){
    let t=new Date()
    let textplace= document.getElementById("span1")
    let hoour=t.getHours()
    let minutes=t.getMinutes()
    let second=t.getSeconds()

    finaltext= hoour+" : "+minutes+" : "+second

    textplace.innerHTML=finaltext
}
setInterval(task5,100)
